﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.Database.Sqlite;
 using Android.Database;
using Android.App;

using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
namespace myApp
{
	[Activity(Label = "test1", MainLauncher = true, Icon = "@mipmap/icon")]
	public class MainActivity : Activity
	{
		SQLiteDatabase db;
		DBOpenHelper1 dbOpenHelper;
		ICursor icur;
		protected string[] items;
		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);
			dbOpenHelper = new DBOpenHelper1(this);
			// Set our view from the "main" layout resource
			SetContentView(Resource.Layout.Main);
			db = dbOpenHelper.ReadableDatabase;
			icur = db.Query("setting", new string[] { "settingID", "pretime" }, " settingID = ? ", new string[] { "1" }, null, null, null);

			// Get our button from the layout resource,
			// and attach an event to it

			items = new string[]{
				"First","Second","Third"
			};
			ListView list = (ListView)FindViewById<ListView>(Resource.Id.listView1);
			list.Adapter = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleListItem1, items);
			list.ItemClick += (object sender, AdapterView.ItemClickEventArgs args) => OnListItemClick(sender, args);
			Button button = (Button)FindViewById(Resource.Id.myButton);
			button.Click += OnButtonClicked;
		}
		protected void OnListItemClick(object sender, EventArgs e)
		{
			AdapterView.ItemClickEventArgs arg = (AdapterView.ItemClickEventArgs)e;
			var t = items[arg.Position];
			Toast.MakeText(this, t, ToastLength.Short).Show();
		}
		void OnButtonClicked(object sender, EventArgs e)
		{
			
			db = dbOpenHelper.WritableDatabase;
			//put content value
			ContentValues cv = new ContentValues();
			cv.Put("pretime", "pretime1");
			cv.Put("frequency", "frequency1");
			cv.Put("alarm", "alarm1");
			/* insert data */
			db.Insert("setting", null, cv);

			Toast.MakeText(this,icur.Count.ToString(), ToastLength.Short).Show();
			db.Close();
		}
	}
}
