﻿using System;
using Android.Database.Sqlite;
using Android.Content;
namespace myApp
{
	public class DBOpenHelper1: SQLiteOpenHelper
	{
		public DBOpenHelper1(Context context)
                : base(context,DATABASE_NAME, null,1)
            {
		}
		public override void OnCreate(SQLiteDatabase db)
		{
			db.ExecSQL(CREATE_TABLE_SETTING);
		}

		public override void OnUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
		{
			db.ExecSQL("DROP TABLE IF EXISTS " + TABLE_SETTING);
			// Creating new tables
			OnCreate(db);
		}

		private static String DATABASE_NAME = "test";
		private static String TABLE_SETTING = "setting";

		// Setting Column Names
		private static  String SETTING_ID = "settingID";
        private static  String SETTING_PERTIME = "pretime";
        private static  String SETTING_FREQUENCY = "frequency";
        private static  String SETTING_ALARM = "alarm";

		private static  String CREATE_TABLE_SETTING = "create table "
            + TABLE_SETTING + "(" + SETTING_ID + " integer primary key autoincrement, "
            + SETTING_PERTIME + " text, " + SETTING_FREQUENCY + " text, "
            + SETTING_ALARM + " text);";
	}
}

